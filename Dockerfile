FROM cargo.dev.caicloud.xyz/qatest/alpine AS builder
COPY . /builder

MAINTAINER fufutest.caicouddiosdasd
FROM cargo.dev.caicloud.xyz/qatest/alpine:gpp 
LABEL version="1.1"
LABEL maintainer="fufu123@caicloud.io"
ENV NAME VAR1
ENV NAME=VAR2
ENV NAME VAR3
RUN mkdir /write_directory
ARG DIRECTORY=/write_directory
ENV VAR_DIR=$DIRECTORY
EXPOSE 80/udp
COPY --from=builder /builder/ .
ADD . /HelloWorld
WORKDIR /HelloWorld
